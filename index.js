var fs = require('fs');
var path = require('path');

module.exports = function(robot, scripts) {
  var scriptsPath;

  scriptsPath = path.resolve(__dirname, 'src');

  fs.exists(scriptsPath, function (exists) {
    if (exists) {
      fs.readdir(scriptsPath, function (err, scriptList) {
        scriptList.forEach(function (script) {
          if (typeof scripts !== 'undefined' && !('*' in scripts)) {
            if (script in scripts) {
              robot.loadFile(scriptsPath, script);
            }
          } else {
            robot.loadFile(scriptsPath, script);
          }
        });
      });
    }
  });
};
