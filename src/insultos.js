//
// Description
//   Have your bot generate a new insult in spanish just for you y
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   hubot insulto — Makes hubot insult you
//
// Notes:
//   None
//
// Author:
//   rbdr
//

module.exports = function (robot) {
  robot.respond(/insulto/i, function (message) {
    robot.http('http://insultos.unlimited.pizza/raw').get()(function (err, res, body) {
      if (err) {
        return message.send('No mereces un insulto');
      }

      message.send(body);
    });
  });
};
