var assert = require('assert');

describe('insultos', function () {
  before(function () {
    this.robot = {
      respond : function () {
        this.arguments = Array.prototype.slice.call(arguments);
      }.bind(this)
    }
  });

  it('should register a respond listener', function () {
    var expected, actual, message;
    require('../src/insultos')(this.robot);

    expected = '/insulto/i';
    actual = this.arguments[0].toString();
    message = 'Expected command to be ' + expected;

    assert.strictEqual(expected, actual, message);
  });
});
